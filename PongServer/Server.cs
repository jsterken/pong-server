﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using PongServer.Models;

namespace PongServer
{
	public class Server
	{
		private const int PortReplyBroadcast = 1336;
		private const int NumBroadcastReplyWorkers = 10;
		private const int NumHandlers = 20;

		private TcpListener tcpListener;
		private UdpClient udpBroodkastListener;
		private UdpClient udpUnicastListener;

		public void Start(int broodkastPort, int statusPort, int udpUnicastPort)
		{
			Console.WriteLine("Server started, listening on port {0} for udp broadcasts", broodkastPort);
			udpBroodkastListener = new UdpClient(broodkastPort) { EnableBroadcast = true };

			// Start up 20 async tasks to reply to broadcasts
			for (int i = 0; i < NumBroadcastReplyWorkers; i++)
				ListenBroadcast();

			Console.WriteLine("Server started, listening on port {0} for data", statusPort);
			tcpListener = new TcpListener(IPAddress.Any, statusPort);
			tcpListener.Start();

			Console.WriteLine("Server started, listening on port {0} for udp unicast", udpUnicastPort);
			udpUnicastListener = new UdpClient(udpUnicastPort);
			for (int i = 0; i < NumHandlers; i++)
			{
				ListenUdpUnicast();
			}

			// Start up 20 async tasks to accept connections
			for (int i = 0; i < NumHandlers; i++)
				Accept();
		}

		private void Accept()
		{
			tcpListener.AcceptTcpClientAsync().ContinueWith(Handle);
		}

		private void ListenBroadcast()
		{
			udpBroodkastListener.ReceiveAsync().ContinueWith(ReplyBroadcast);
		}

		private void ListenUdpUnicast()
		{
			udpUnicastListener.ReceiveAsync().ContinueWith(ParseUnicast);
		}

		private void ReplyBroadcast(Task<UdpReceiveResult> udpReceiveTask)
		{
			byte[] message = Encoding.ASCII.GetBytes("HEY!");

			udpReceiveTask.Result.RemoteEndPoint.Port = PortReplyBroadcast;
			using (UdpClient sendClient = new UdpClient())
			{
				sendClient.Send(message, message.Length, udpReceiveTask.Result.RemoteEndPoint);
			}

			ListenBroadcast();
		}

		private void ParseUnicast(Task<UdpReceiveResult> udpReceiveTask)
		{
			UdpReceiveResult receiveResult = udpReceiveTask.Result;

			UdpMessageParser messageParser = new UdpMessageParser(receiveResult.RemoteEndPoint);

			messageParser.UpdatePlayerPosition(receiveResult.Buffer);

			ListenUdpUnicast();
		}

		private void Handle(Task<TcpClient> acceptTask)
		{
			try
			{
				TcpClient client = acceptTask.Result;
				StreamWriter writer = new StreamWriter(client.GetStream());

				writer.WriteLine("YOLO");
				writer.Flush();

				Console.WriteLine("Client with IP {0} connected", ((IPEndPoint)client.Client.RemoteEndPoint).Address);
				new TcpMessageParser().HandleClientComm(client);
				Console.WriteLine("Connection with IP {0} closed", ((IPEndPoint)client.Client.RemoteEndPoint).Address);
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
				Console.WriteLine(e.StackTrace);
			}


			// Start a new task to accept the next request
			Accept();
		}

		public static void SendGameInfo(object gameObject)
		{
			Game game = (Game)gameObject;

			Player winner = SimulateGame(game);

			foreach (Player player in game.Players)
			{
				string winnerMessage = string.Format("5.{0}", winner.Id);

				StreamWriter writer = new StreamWriter(player.TcpClient.GetStream());
				writer.Write(winnerMessage);

			}
		}

		public static Player SimulateGame(Game game)
		{
			Player challenger = game.Players[0];
			Player champion = game.Players[1];
			Stopwatch stopwatch = Stopwatch.StartNew();

			while (challenger.Score < Game.ScoreToWin && champion.Score < Game.ScoreToWin)
			{
				Console.WriteLine("X = {0} Y = {1}", game.Ball.Position.X, game.Ball.Position.Y);
				if (game.Ball.Position.X < Position.XConstraints.Minimum)
				{
					champion.Score++;
					SendScoreInfo(challenger, champion);
				}
				if (game.Ball.Position.X > Position.XConstraints.Maximum)
				{
					challenger.Score++;
					SendScoreInfo(challenger, champion);
				}

				const int sleepTime = 10;
				Thread.Sleep(sleepTime);

				UpdateBallPosition(challenger, champion, game.Ball, sleepTime, (int)stopwatch.ElapsedMilliseconds);
				stopwatch.Restart();

				using (var udpClient = new UdpClient())
				{
					byte[] message =
						new ASCIIEncoding().GetBytes(string.Format("{0}.{1}.{2}.{3}.{4}.{5}.{6}", game.Id, challenger.Id,
							challenger.Position.X, challenger.Position.Y, champion.Id, champion.Position.X,
							champion.Position.Y));

					udpClient.EnableBroadcast = true;
					udpClient.Send(message, message.Length, new IPEndPoint(IPAddress.Broadcast, 1338));
				}
			}
			Player winner = challenger.Score > champion.Score ? challenger : champion;
			return winner;
		}

		private static void UpdateBallPosition(Player challenger, Player champion, Ball ball, int sleepTime, int elapsedMilliseconds)
		{
			var xVelocity = ball.Velocity.X * sleepTime / elapsedMilliseconds;
			var yVelocity = ball.Velocity.Y * sleepTime / elapsedMilliseconds;

			var newBallX = ball.Position.X + xVelocity;
			var newBallY = ball.Position.Y + yVelocity;

			if (newBallX <= Position.XConstraints.Minimum || newBallX >= Position.XConstraints.Maximum)
			{
				//ball goes past paddle
				if ((newBallX <= Position.XConstraints.Minimum && challenger.Position.Y > ball.Position.Y &&
					 challenger.Position.Y + Player.PaddleSize < ball.Position.Y) ||
					(newBallX >= Position.XConstraints.Maximum && champion.Position.Y > ball.Position.Y &&
					 champion.Position.Y + Player.PaddleSize < ball.Position.Y))
				{
					return;
				}

				xVelocity *= -1;
				ball.Velocity.X *= -1;
				newBallX = newBallX + xVelocity;
			}

			if (newBallY <= Position.YConstraints.Minimum || newBallY >= Position.YConstraints.Maximum)
			{
				//ball goes past top
				yVelocity *= -1;
				ball.Velocity.Y *= -1;
				newBallY = newBallY + yVelocity;
			}

			// Finally, copy newBall to ball
			ball.Position.X = newBallX;
			ball.Position.Y = newBallY;
		}

		private static void SendScoreInfo(Player challenger, Player champion)
		{
			string scoreMessage = string.Format("4.{0}.{1}.{2}.{3}", challenger.Id, challenger.Score, champion.Id,
				champion.Score);

			StreamWriter challengerWriter = new StreamWriter(challenger.TcpClient.GetStream());
			challengerWriter.Write(scoreMessage);

			StreamWriter championWriter = new StreamWriter(champion.TcpClient.GetStream());
			championWriter.Write(scoreMessage);
			//challenger.TcpClient.Client.Send(scoreMessage, scoreMessage.Length);
		}
	}
}