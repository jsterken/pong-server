﻿using System;
using System.Threading;

namespace PongServer
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            new Server().Start(1336, 1337, 1338);
            while (true)
            {
                //if (Console.KeyAvailable)
                //    break;

                Console.Out.Write("\rNot blocking (yet)? |");
                Thread.Sleep(100);
                Console.Out.Write("\rNot blocking (yet)? /");
                Thread.Sleep(100);
                Console.Out.Write("\rNot blocking (yet)? -");
                Thread.Sleep(100);
                Console.Out.Write("\rNot blocking (yet)? \\");
                Thread.Sleep(100);
                Console.Out.Write("\rNot blocking (yet)? |");
                Thread.Sleep(100);
                Console.Out.Write("\rNot blocking (yet)? /");
                Thread.Sleep(100);
                Console.Out.Write("\rNot blocking (yet)? -");
                Thread.Sleep(100);
                Console.Out.Write("\rNot blocking (yet)? \\");
                Thread.Sleep(100);
            }
        }
    }
}