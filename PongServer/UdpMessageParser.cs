﻿using System;
using System.Net;
using PongServer.Models;

namespace PongServer
{
    public class UdpMessageParser
    {
        private Player player;
        public UdpMessageParser(IPEndPoint endPoint)
        {
            string ipAddress = endPoint.Address.ToString();
            player = Database.FindPlayerByIpAddress(ipAddress);
        }

        public void UpdatePlayerPosition(byte[] buffer)
        {
            int movement = BitConverter.ToInt32(buffer, 0);
            int playerPosition = player.Position.Y;

            playerPosition += movement;

            if (playerPosition > Position.YConstraints.Maximum) playerPosition = Position.YConstraints.Maximum;
            if (playerPosition < Position.YConstraints.Minimum) playerPosition = Position.YConstraints.Minimum;

            player.Position.Y = playerPosition;

            Console.WriteLine("Position updated to X = {0} Y = {1}", player.Position.X, player.Position.Y);
        }
    }
}