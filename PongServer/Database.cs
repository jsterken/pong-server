﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using PongServer.Models;

namespace PongServer
{
    public class Database
    {
        public static List<Player> Players = new List<Player>();
        public static List<Game> ActiveGames = new List<Game>();

        public static Player RegisterNewPlayer(string ipAddress, string name, TcpClient tcpClient)
        {
            var player = new Player
            {
                IpAddress = ipAddress,
                Name = name,
                Status = Status.New,
                TcpClient = tcpClient
            };
            Players.Add(player);
            player.Id = Players.FindIndex(p => p == player);

            return player;
        }

        public static void InitGame(Player challenger, Player champion)
        {
            var game = new Game
            {
                Ball = new Ball
                {
                    Position = new Position {X = 64, Y = 48},
                    Heading = 90,
                    Velocity = new Velocity { X = 10, Y = 5} //TODO: init velocity
                },
                Players = new List<Player>
                {
                    challenger,
                    champion
                }
            };
            ActiveGames.Add(game);
            game.Id = ActiveGames.FindIndex(g => g == game);
            challenger.Status = Status.InGame;
            champion.Status = Status.InGame;

            new Thread(Server.SendGameInfo).Start(game);
        }

        public static IEnumerable<Player> ListAvailablePlayersExceptCurrentPlayer(Player player)
        {
            return Players.Where(p => p.Status == Status.New && p != player);
        } 

        public static void RemovePlayerByIpAddress(string ipAddress)
        {
            var player = Players.Select(p => p).SingleOrDefault(p => p.IpAddress == ipAddress);
            Players.Remove(player);
        }

        public static Player FindPlayerById(int id)
        {
            return Players.Select(p => p).Single(p => p.Id == id);
        }

        public static Player FindPlayerByName(string name)
        {
            return Players.Select(p => p).Single(p => p.Name == name);
        }

        public static Player FindPlayerByIpAddress(string ipAddress)
        {
            return Players.Select(p => p).SingleOrDefault(p => p.IpAddress == ipAddress);
        }

        public static void UpdatePlayerPosition(int playerId, Position position)
        {
            Player player = FindPlayerById(playerId);
            player.Position = position;
        }

        public static List<Player> SearchPlayersThatAreNotInGame()
        {
            return Players.FindAll(p => p.Status == Status.New);
        }
    }
}