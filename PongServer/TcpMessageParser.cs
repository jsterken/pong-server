using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using PongServer.Models;
using System;

namespace PongServer
{
	public class TcpMessageParser
	{
		public Player Player;
		public void HandleClientComm(object client)
		{
			TcpClient tcpClient = (TcpClient)client;
			NetworkStream clientStream = tcpClient.GetStream();

			while (true)
			{
				int bytesRead = 0;
				byte[] message = new byte[4096];
				try
				{
					//blocks until a client sends a message
					bytesRead = clientStream.Read(message, 0, 4096);
					if (Player != null)
					{
						Player.Command = message;
						Player.BytesRead = bytesRead;
					}
					string returnMessage = HandleMessage(bytesRead, message, tcpClient);
					if (returnMessage != null)
					{
						StreamWriter writer = new StreamWriter(clientStream);
						writer.Write(returnMessage);
						writer.Flush();
					}
				}
				catch
				{
					//a socket error has occurred
					Database.RemovePlayerByIpAddress(((IPEndPoint)tcpClient.Client.RemoteEndPoint).Address.ToString());
					tcpClient.Close();
					break;
				}

				if (bytesRead == 0)
				{
					//the client has disconnected from the server
					Database.RemovePlayerByIpAddress(((IPEndPoint)tcpClient.Client.RemoteEndPoint).Address.ToString());
					break;
				}
				Thread.Sleep(10);
				if (Player != null)
				{
					Player.Command = null;
					Player.BytesRead = 0;
				}
			}
		}

		private string HandleMessage(int bytesRead, byte[] messageBytes, TcpClient tcpClient)
		{
			switch (messageBytes[0])
			{
				case 48:
					return RegisterNewPlayer(bytesRead, messageBytes, tcpClient);
				case 49:
					return AvailablePlayers;
				case 50:
					return ChallengeChampion(bytesRead, messageBytes);
			}
			return null;
		}

		private string ChallengeChampion(int bytesRead, byte[] messageBytes)
		{
			var id = Int32.Parse(ConstructStringFromBytes(bytesRead, 1, messageBytes));
			if (id == 1337)
				return "3NJET";
			if (id == 9001)
				return "3DA";

			var champion = Database.FindPlayerById(id);

			if (champion.Status == Status.InGame) return "3NJET";

			var championStream = champion.TcpClient.GetStream();
			StreamWriter writer = new StreamWriter(championStream);

			writer.Write("2{0}.{1}", Player.Id, Player.Name);
			writer.Flush();
			byte[] championResponse;
			int championBytesRead;
			while (true)
			{
				if (champion.Command == null) continue;
				championResponse = champion.Command;
				championBytesRead = champion.BytesRead;
				break;
			}
			var championResponseString = ConstructStringFromBytes(championBytesRead, 0, championResponse);
			if (championResponseString == "3DA")
			{
				Database.InitGame(Player, champion);
				return "3DA";
			}
			return "3NJET";
		}

		private string AvailablePlayers
		{
			get
			{
				StringBuilder playerString = new StringBuilder("1");
				foreach (var player in Database.ListAvailablePlayersExceptCurrentPlayer(Player))
				{
					playerString.Append(string.Format("{0}.{1}\n", player.Id, player.Name));
				}
				return playerString.ToString();
			}
		}

		private string RegisterNewPlayer(int bytesRead, byte[] messageBytes, TcpClient tcpClient)
		{
			var clientIpAddress = ((IPEndPoint)tcpClient.Client.RemoteEndPoint).Address.ToString();

			//Database.RegisterNewPlayer(clientIpAddress, "Henk", tcpClient);
			//Database.RegisterNewPlayer(clientIpAddress, "Piet", tcpClient);

			var name = ConstructStringFromBytes(bytesRead, 1, messageBytes);
			if (Player == null)
			{
				Player = Database.RegisterNewPlayer(clientIpAddress, name, tcpClient);
				return "0KTHXBAI";
			}
			return "0MAGNIET";
		}

		private static string ConstructStringFromBytes(int bytesRead, int offset, byte[] messageBytes)
		{
			byte[] stringBytes = new byte[bytesRead - offset];
			for (int i = offset; i < bytesRead; i++)
			{
				stringBytes[i - offset] = messageBytes[i];
			}
			return new ASCIIEncoding().GetString(stringBytes);
		}
	}
}