﻿namespace PongServer.Models
{
    public class Position
    {
        public static readonly PositionConstraints YConstraints = new PositionConstraints {Minimum = 0, Maximum = 96};
        public static readonly PositionConstraints XConstraints = new PositionConstraints {Minimum = 0, Maximum = 128};
        public int X { get; set; }

        public int Y { get; set; }
    }
}