﻿using System.Collections.Generic;

namespace PongServer.Models
{
    public class Game
    {
        public const int ScoreToWin = 10;
        public int Id { get; set; }

        public List<Player> Players { get; set; }

        public Ball Ball { get; set; }
    }
}