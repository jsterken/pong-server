﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PongServer.Models
{
    public class Velocity
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
