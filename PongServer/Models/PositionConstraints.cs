﻿namespace PongServer.Models
{
    public class PositionConstraints
    {
        public int Minimum { get; set; }
        public int Maximum { get; set; }
    }
}
