﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PongServer.Models
{
    public class Ball
    {
        public Position Position { get; set; }
        public int Heading { get; set; }
        public Velocity Velocity { get; set; }
    }
}
