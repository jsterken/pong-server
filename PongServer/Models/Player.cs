﻿using System.Net.Sockets;

namespace PongServer.Models
{
    public enum Status
    {
        New,
        InGame
    }

    public class Player
    {
        public const int PaddleSize = 10;
        public Player()
        {
            Status = Status.New;
            Position = new Position {X = 0, Y = 0};
        }
        public int Id { get; set; }

        public string Name { get; set; }

        public string IpAddress { get; set; }

        public Status Status { get; set; }

        public Position Position { get; set; }

        public int Score { get; set; }

        public TcpClient TcpClient { get; set; }

        public byte[] Command { get; set; }

        public int BytesRead { get; set; }
    }
}